# KDFlow-Modelo
_Modelo de flujo de versionado GIT_

Este repositorio contiene un ejemplo de flujo de versionado de GIT según la definición de KDFlow.

KDFlow es un flujo de trabajo con repositorios GIT inspirado en GIT-Flow y ajustado para las necesidades de negocio de KDSoft.

Este repositorio toma como base un template de proyecto web gratuito, con el proposito de explicar la implementación del flujo. Ni el material original, ni ninguna modificación hecha aquí, tienen como objetivo formar parte de ningún desarrollo, **ni considerarse base o ejemplo de buenas prácticas ni definiciones para el desarrollo de software en sí**, solamente están como ejemplo de flujo de versionado. 

El proyecto original se puede ver en https://templated.co/hielo. La licencia de uso original se puede ver en el archivo LICENCE.txt

**LOS CRÉDITOS CORRESPONDEN A SUS RESPECTIVOS AUTORES, NO SE AUTORIZA EL USO NI LA REPRODUCCIÓN TOTAL O PARCIAL DEL CÓDIGO DE ESTE REPOSITORIO PARA OTROS USOS DIFERENTES A EXPLICAL EL USO E IMPLEMENTACIÓN DE KDLFOW**

<hr>

Para poder aprovechar el contenido, se recomienda revisar bien la estructura de branches y los historiales de cambios entendiendo cada una de las acciones tomadas en cada situación.

Se puede ver la definición del flujo en este documento: 
https://docs.google.com/document/d/1mILy_NXGWBrltQ-PcFX0xFbgJSJVlf4Z98WyYW4Vwxk

y un ejemplo explicativo tomando este mismo repositorio como ejemplo en esta presentación:
https://docs.google.com/presentation/d/1_-yTuoRp2sJ-Wfpx-S2jBOQrJfsyEfvYbct03ci2Md8

<hr>

## Implementación Funcional
Se tomaron las siguientes definiciones funcionales ficticias para aplicar los cambios en este proyecto, listadas en orden de desarrollo e implementación:
1.  Crear un sitio web a partir de un template (referencia de gestión #005). QA detecta un error y se corrige.
2. Agregar en el menu de navegación un enlace a la pagina de kinetic (referencia de gestión #012).
3. Modificar el titulo de la pagina generic (referencia de gestión #022).
4. Desarrollar nueva pagina (referencia de gestión #045).
5. Bug de links rotos y faltantes en footer (referencia de gestión #038).
6. Hotfix: el link al sitio de kinetic esta equivocado! (referencia de gestión #055).

<hr>

## Implementación Técnica del flujo

### _**PRE-IMPLEMENTACION**_

1. Creación del repo inicial
2. Implementación del template (ref #005)
    * Se crea el brach del feature: _features/#005-template-inicial_ **desde el MASTER**
    * Se desarrollan y se confirman los cambios
3. Se implementan los cambios en el ambiente de testing
    * Se crea el branch de testing: _testing_ **desde el MASTER**, y se hace el merge desde el branch del feature _features/#005-template-inicial_
4. QA detecta un error
    * Se desarrolla la corrección en el branch del feature _features/#005-template-inicial_
    * Se vuelve a desplegar en testing haciendo el merge desde el branch del feature
5. Se aprueba y se implementa en producción
    * Se crea el branch de release: _Release/v1.0.0.0-implementacion-inicial_ **desde el MASTER**, con un identificador de versión o release
    * Se hace el merge desde el brach de la feature (o los branches si implementamos mas de un feature) que se quiere implementar en este release: _features/#005-template-inicial_
    * Se hace el merge **al MASTER, desde el branch de release** (junto con la implementación real)


### _**POST Primera Implementación (v1.0)**_

1. Cambio en el menu (ref #012)
    * Se crea el brach del feature: _features/#012-cambios-menu_ **desde el MASTER**
    * Se desarrollan y se confirman los cambios
2. Se implementan los cambios en el ambiente de testing
    * Se hace el merge desde el branch del feature _features/#012-cambios-menu_
3. Se desarrolla en paralelo el cambio del título de la página de genéricos (ref #022)
    * Se crea el brach del feature: _features/#022-titulo-genericos_ **desde el MASTER**
    * Se desarrollan y se confirman los cambios
4. Se implementan los cambios en el ambiente de testing
    * Se hace el merge desde el branch del feature _features/#022-titulo-genericos_
5. Se aprueban los cambios y se implementa en producción una nueva release
    * Se crea el branch de release: _Release/v1.1.0.0-menu-genericos_ **desde el MASTER**, con un identificador de versión o release
    * Se hace el merge desde los branches que se quiere implementar en este release: 
        * _features/#012-cambios-menu_
        * _features/#022-titulo-genericos_
    * Se hace el merge **al MASTER, desde el branch de release** (junto con la implementación real)

### _**POST Primera Implementación (v1.1)**_
1. Desarrollo nueva página a partir de genérico (ref #045)
    * Se crea el brach del feature: _features/#045-nueva-pagina-catalogo_ **desde el MASTER**
    * Se desarrollan y se confirman los cambios
2. Se implementan los cambios en el ambiente de testing
    * Se hace el merge desde el branch del feature _features/#045-nueva-pagina-catalogo_
3. En paralelo se hace la corrección de bug de links en los footers (ref #038)
    * Se crea le branch del bug: _bug/#038-links-rotos-footer_ **desde el MASTER**
    * Se desarrollan y se confirman los cambios
4. Se implementan los cambios en el ambiente de testing
    * Se hace el merge desde el branch del bug _bug/#038-links-rotos-footer_
5. Por comunicación en el equipo se implementa el mismo cambio en la pagina nueva para evitar generar un nuevo bug
    * Se desarrolla la corrección en el branch del feature _features/#045-nueva-pagina-catalogo_
    * Se vuelve a desplegar en testing haciendo el merge desde el branch del feature
6. Hotfix en producción por bug de dato de contacto a pagina de kinetic (ref #055)
    * Se crea le branch del hotfix: _hotfix/#055-error-pagina-kinetic_ **desde el MASTER**
    * Se desarrolla la solución y se confirman los cambios
    * Se implementa directo en producción (sin plan de release), haciendo el merge **al MASTER desde el branch del hotfix:** _hotfix/#055-error-pagina-kinetic_
7. Se aprueba en testing los cambios de la nueva pagina y el bug
    * Se crea el branch de release: _Release/v1.2.0.0-bugfixes-pagina-catalogo_ **desde el MASTER**, con un identificador de versión o release (_esto se hace a posterior de haber implementado el hotfix_)
    * Se hace el merge desde los branches que se quiere implementar en este release: 
        * _features/#045-nueva-pagina-catalogo_
        * _bug/#038-links-rotos-footer_
    * Se corrigen los conflictos de merge
    * Se hace el merge **al MASTER, desde el branch de release** (junto con la implementación real)

<hr>